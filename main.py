#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  test.flac
#  
#  Copyright 2022 jni <jni@devuan>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import PySimpleGUI as sg

value = ""

sg.theme('DarkAmber')   # Add a touch of color
# All the stuff inside your window.
layout = [  [sg.Text('Calculator')],
            [sg.Input(key='Text1')],
            [sg.Button('1'), sg.Button('2'), sg.Button('3'), sg.T(''), sg.Button('+',size=(1,1)), sg.Button('/',size=(1,1)), sg.Button('DEL',size=(1,1))],
            [sg.Button('4'), sg.Button('5'), sg.Button('6'), sg.T(''), sg.Button('-',size=(1,1)), sg.Button('.',size=(1,1))],
            [sg.Button('7'), sg.Button('8'), sg.Button('9'), sg.T(''), sg.Button('*',size=(1,1)), sg.Button('C',size=(1,1))],
            [sg.Button('(', size=(1,1)), sg.Button('0', size=(1,1)), sg.Button(')', size=(1,1))],
            [sg.T('')],
            [sg.Button('=',size=(1,1))]]


# Create the Window
window = sg.Window('Window Title', layout)
# Event Loop to process "events" and get the "values" of the inputs
while True:
    event, values = window.read()
    if event == sg.WIN_CLOSED: # if user closes window or clicks cancel
        break
    elif event == '=':
        if value == "":
            pass
        else:
            value = str(eval(value))
    elif event == '+':
        value += " + "
    elif event == '-':
        value += " - "
    elif event == '/':
        value += " / "
    elif event == '*':
        value += " * "
    elif event == '.':
        if value[-1] == ".":
            pass
        else:
            value += "."
    elif event == '(':
        value += " ("
    elif event == ')':
        value += ") "
    elif event == 'DEL':
        if len(value) > 0:
            value = value[:-1]
        else:
            pass 
    elif event == 'C':
        value = ""
    elif event == '1':
        value += "1"
    elif event == '2':
        value += "2"
    elif event == '3':
        value += "3"
    elif event == '4':
        value += "4"
    elif event == '5':
        value += "5"
    elif event == '6':
        value += "6"
    elif event == '7':
        value += "7"
    elif event == '8':
        value += "8"
    elif event == '9':
        value += "9"
    elif event == '0':
       value += "0"
    window.Element('Text1').Update("".join(value))
window.close()
